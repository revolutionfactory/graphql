module GraphQL.Constructors where

import GHC.TypeLits (KnownSymbol, symbolVal)
import Control.Arrow ((>>>))
import Control.Monad ((>=>))
import Control.Monad.State (StateT, evalState, get, put)
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))
import Data.Bitraversable (bitraverse)
import Data.Coerce (coerce)
import Data.Foldable (toList)
import Data.List (sortOn)
import Data.Proxy (Proxy(Proxy))
import Data.String (fromString)
import Data.Text (Text, unpack)
import Data.Typeable (Typeable, showsTypeRep, typeRep)
import GHC.Generics
import GraphQL.Optics
import GraphQL.Types
import Lens.Micro
import Safe (tailMay, readMay)
import qualified Control.Newtype as N
import qualified Data.Aeson as A
import qualified Data.Aeson.Types as A
import qualified Data.HashMap.Strict as HM

class ToValue a where
  toValue :: a -> Value
  default toValue :: (Generic a, ToValue' (Rep a)) => a -> Value
  toValue = toValue' . from

instance ToValue Text where
  toValue = StringValue

instance ToValue a => ToValue [a] where
  toValue = ListValue . coerce . fmap toValue

class ToValue' f where toValue' :: f p -> Value
type Pair = (Text,Value)
class ToPairs f where toPairs :: [Pair] -> f a -> [Pair]

instance ToValue' f => ToValue' (D1 c f) where
  toValue' = toValue' . unM1

instance ToPairs (b :*: c) => ToValue' (C1 a (b :*: c)) where
  toValue' = ObjectValue . coerce . toPairs [] . unM1

instance (KnownSymbol field, ToValue' value) => ToValue' (C1 a (S1 ('MetaSel ('Just field) su ss ds) value)) where
  toValue' = ObjectValue . coerce . (:[]) . toPair . unM1

instance (ToPairs a, ToPairs b) => ToPairs (a :*: b) where
  toPairs ps (x :*: y) = toPairs (toPairs ps y) x

instance (KnownSymbol field, ToValue' value) => ToPairs (S1 ('MetaSel ('Just field) su ss ds) value) where
  toPairs ps s = toPair s : ps

instance (ToValue' a, ToValue' b) => ToValue' (a :+: b) where
  toValue' = \case
    L1 x -> toValue' x
    R1 x -> toValue' x

instance KnownSymbol constructor => ToValue' (C1 ('MetaCons constructor f s) U1) where
  toValue' _ = EnumValue $ fromString $ symbolVal $ Proxy @constructor

instance ToValue a => ToValue' (Rec0 a) where toValue' = toValue . unK1

toPair
  :: forall field su ss ds value p
  . (KnownSymbol field, ToValue' value)
  => S1 ('MetaSel ('Just field) su ss ds) value p -> Pair
toPair = (fromString $ symbolVal $ Proxy @field ,) . toValue' . unM1

newtype WithJSON a = WithJSON a
instance N.Newtype (WithJSON a) a
instance ToJSON a => ToValue (WithJSON a) where
  toValue = toValue . toJSON . N.unpack

instance ToValue A.Value where
  toValue = \case
    A.Object x -> ObjectValue $ coerce $ (fmap . fmap) toValue $ HM.toList x
    A.Array  x -> ListValue $ List $ fmap toValue $ toList x
    A.String x -> StringValue x
    A.Number x -> FloatValue x
    A.Bool   x -> BooleanValue x
    A.Null     -> NullValue

numberedAlias :: [Field] -> [Field]
numberedAlias = flip evalState startingIndex . traverseOf (traversed . alias) t
  where
  t = const do
    i <- getModify (+ 1)
    pure $ pure $ fromString $ numberedAliasPrefix : show i

newtype NumberedAliases a = NumberedAliases [a] deriving (Show)
instance (Typeable a, FromJSON a) => FromJSON (NumberedAliases a) where
  parseJSON
    =   objectExpected
    >=> (HM.toList >>> traverse (bitraverse parseIndex parseJSON))
    >=> (sortOn fst >>> fmap snd >>> NumberedAliases >>> pure)
    where
    t = showsTypeRep $ typeRep $ Proxy @(NumberedAliases a)
    objectExpected :: A.Value -> A.Parser A.Object
    objectExpected = \case
      A.Object x -> pure x
      x -> fail $ "While parsing " <> t (" expected a JSON Object but received: " <> show x)
    parseIndex :: Text -> A.Parser Int
    parseIndex = unpack >>> \xs -> (tailMay >=> readMay) >>> maybe (fail $ "Could not converte index \"" <> xs <> "\" to an Int.") pure $ xs

numberedAliasPrefix :: Char
numberedAliasPrefix = 'i'

startingIndex :: Int
startingIndex = 0

getModify :: Monad m => (s -> s) -> StateT s m s
getModify f = do
  x <- get
  put $ f x
  pure x

singleArgument :: Applicative m => Name -> Value -> m Arguments
singleArgument name value = pure $ Arguments $ pure $ Argument name value

selectionSetFragment :: FragmentName -> TypeCondition -> SelectionSet -> (Definition,Selection)
selectionSetFragment fragmentName typeCondition selectionSet =
  ( DefinitionExecutableDefinition $ ExecutableFragmentDefinition $ FragmentDefinition fragmentName typeCondition Nothing selectionSet
  , SelectionFragmentSpread $ FragmentSpread fragmentName Nothing
  )

mutation :: Name -> [Selection] -> Definition
mutation name
  = DefinitionExecutableDefinition
  . ExecutableOperationDefinition
  . OperationDefinition Mutation (Just name) Nothing Nothing
  . SelectionSet

field :: Name -> [Selection] -> Selection
field name = SelectionField . Field Nothing name Nothing Nothing . pure . SelectionSet

