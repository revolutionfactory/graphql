module GraphQL.Types where

import Control.Newtype (Newtype)
import Data.Bool (Bool)
import Data.Maybe (Maybe)
import Data.Scientific (Scientific)
import Data.String (IsString(fromString))
import Data.Text (Text)
import GHC.Integer (Integer)


newtype Document = Document [Definition]
instance Newtype Document [Definition]

data Definition
  = DefinitionExecutableDefinition ExecutableDefinition
  -- | TypeSystemDefinition
  -- | TypeSystemDefinition

data ExecutableDefinition
  = ExecutableOperationDefinition OperationDefinition
  | ExecutableFragmentDefinition FragmentDefinition

data OperationDefinition
  = OperationDefinition
    OperationType
    (Maybe Name)
    (Maybe VariableDefinitons)
    (Maybe Directives)
    SelectionSet
  | OperationSelectionSet SelectionSet

data OperationType
  = Query
  | Mutation
  | Subscription

newtype Name = Name Text deriving (IsString)

newtype VariableDefinitons = VariableDefinitons [VariableDefiniton]
instance Newtype VariableDefinitons [VariableDefiniton]

data VariableDefiniton = VariableDefiniton Variable Type (Maybe DefaultValue)

newtype Variable = Variable Name deriving (IsString)

data Type
  = TypeNamedType NamedType
  | TypeListType ListType
  -- | NonNullType

newtype ListType = ListType [Type]
instance Newtype ListType [Type]

newtype DefaultValue = DefaultValue Value

newtype Directives = Directives [Directive]
instance Newtype Directives [Directive]

data Directive = Directive Name (Maybe Arguments)

newtype SelectionSet = SelectionSet [Selection]
instance Newtype SelectionSet [Selection]

data Selection
  = SelectionField Field
  | SelectionFragmentSpread FragmentSpread
  -- | SelectionInlineFragment InlineFragment
instance IsString Selection where fromString = SelectionField . fromString

data Field = Field
  (Maybe Alias)
  Name
  (Maybe Arguments)
  (Maybe Directives)
  (Maybe SelectionSet)
instance IsString Field where fromString x = Field Nothing (fromString x) Nothing Nothing Nothing

data FragmentSpread = FragmentSpread FragmentName (Maybe Directives)

newtype FragmentName = FragmentName Name deriving (IsString)
instance Newtype FragmentName Name

data FragmentDefinition = FragmentDefinition FragmentName TypeCondition (Maybe Directives) SelectionSet

newtype TypeCondition = TypeCondition NamedType deriving (IsString)

newtype NamedType = NamedType Name deriving (IsString)
instance Newtype NamedType Name

newtype Alias = Alias Text deriving (IsString)

newtype Arguments = Arguments [Argument]
instance Newtype Arguments [Argument]

data Argument = Argument Name Value

data Value
  = BooleanValue  Bool
  | EnumValue     Name
  | FloatValue    Scientific
  | IntValue      Integer
  | ListValue     List
  | NullValue
  | ObjectValue   Object 
  | StringValue   Text
  | VariableValue Variable

newtype List = List [Value]
instance Newtype List [Value]

newtype Object = Object [ObjectField]
instance Newtype Object [ObjectField]

newtype ObjectField = ObjectField (Name,Value)

