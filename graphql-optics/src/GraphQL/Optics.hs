module GraphQL.Optics where

import Lens.Micro
import GraphQL.Types


alias :: Lens' Field (Maybe Alias)
alias = lens
  (\(Field x _ _ _ _) -> x)
  \(Field _ a b c d) x -> Field x a b c d

