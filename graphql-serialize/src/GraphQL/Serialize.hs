module GraphQL.Serialize where

import Control.Newtype (Newtype(unpack))
import Data.Scientific (Scientific)
import Data.String (IsString(fromString))
import Data.Text hiding (unpack)
import GraphQL.Types

-- Should be updated to use Reader to specify the pretty printing format. ex: comma no comma

class Serialize a where serialize :: a -> Text

infixr 6 <:>
(<:>) :: (Serialize a, Serialize b) => a -> b -> Text
x <:> y = serialize x <> serialize y

infixr 6 <->
(<->) :: (Serialize a, Serialize b) => a -> b -> Text
(<->) = seperate " "

infixr 6 <^>
(<^>) :: (Serialize a, Serialize b) => a -> b -> Text
(<^>) = seperate "\n"

seperate :: (Serialize a, Serialize b) => String -> a -> b -> Text
seperate s x y = serialize x <> fromString s <> serialize y

(<?) :: (Serialize a, Serialize b) => a -> Maybe b -> Text
(<?) x = maybe (serialize x) (x <->)

(?>) :: (Serialize a, Serialize b) => Maybe a -> b -> Text
x ?> y = maybe (serialize y) (<-> y) x

intercalate' :: (Newtype n [a], Serialize a) => Text -> n -> Text
intercalate' x = intercalate x . fmap serialize . unpack

commaSpace :: IsString a => a
commaSpace = fromString ", "

brackets :: (Newtype n [a], Serialize a) => Char -> Char -> n -> Text
brackets x y z = x `cons` intercalate' commaSpace z `snoc` y

parentheses :: (Newtype n [a], Serialize a) => n -> Text
parentheses = brackets '(' ')'

curlies :: (Newtype n [a], Serialize a) => n -> Text
curlies = brackets '{' '}'

squares :: (Newtype n [a], Serialize a) => n -> Text
squares = brackets '[' ']'

newlines :: (Newtype n [a], Serialize a) => n -> Text
newlines = intercalate' $ fromString "\n"

instance Serialize String where
  serialize = pack

instance Serialize Text where
  serialize = id

instance Serialize Document where
  serialize = newlines

instance Serialize Definition where
  serialize = \case
    DefinitionExecutableDefinition x -> serialize x

instance Serialize ExecutableDefinition where
  serialize = \case
    ExecutableOperationDefinition x -> serialize x
    ExecutableFragmentDefinition x -> serialize x

instance Serialize OperationDefinition where
  serialize = \case
    OperationDefinition operationType name variableDefinitons directives selectionSet ->
      operationType <? name <? variableDefinitons <? directives <-> selectionSet
    OperationSelectionSet x -> serialize x

instance Serialize OperationType where
  serialize = \case
    Query        -> fromString "query"
    Mutation     -> fromString "mutation"
    Subscription -> fromString "subscription"

instance Serialize Name where
  serialize (Name x) = x

instance Serialize Directives where
  serialize = intercalate' $ fromString "\n"

instance Serialize Directive where
  serialize (Directive name arguments) = "@" <:> name <? arguments

instance Serialize Arguments where
  serialize = parentheses

instance Serialize Argument where
  serialize (Argument name value) = name <-> ":" <-> value

instance Serialize SelectionSet where
  serialize = curlies

instance Serialize Selection where
  serialize = \case
    SelectionField          x -> serialize x
    SelectionFragmentSpread x -> serialize x

instance Serialize FragmentDefinition where
  serialize (FragmentDefinition fragmentName typeCondition directives selectionSet) = 
    "fragment" <-> fragmentName <-> typeCondition <? directives <-> selectionSet

instance Serialize FragmentName where
  serialize = serialize . unpack

instance Serialize FragmentSpread where
  serialize (FragmentSpread fragmentName directives) = "..." <:> fragmentName <? directives

instance Serialize TypeCondition where
  serialize (TypeCondition x) = "on" <-> x

instance Serialize NamedType where
  serialize = serialize . unpack

instance Serialize Field where
  serialize (Field alias name arguments directives selectionSet) = alias ?> name <? arguments <? directives <? selectionSet

instance Serialize Alias where
  serialize (Alias name) = name <:> ":"

instance Serialize Value where
  serialize = \case
    BooleanValue  x -> serialize x
    EnumValue     x -> serialize x
    FloatValue    x -> serialize x
    IntValue      x -> serialize x
    ListValue     x -> squares x
    NullValue       -> serialize "null"
    ObjectValue   x -> serialize x
    StringValue   x -> '"' `cons` serialize x `snoc` '"'
    VariableValue x -> serialize x

instance Serialize Bool where
  serialize = serialize . \case
    True  -> "true"
    False -> "false"

instance Serialize Scientific where
  serialize = serialize . show

instance Serialize Integer where
  serialize = serialize . show

instance Serialize List where
  serialize = squares

instance Serialize Object where
  serialize = curlies

instance Serialize ObjectField where
  serialize (ObjectField (name,value)) = name <:> ":" <:> value

instance Serialize Variable where
  serialize (Variable name) = "$" <:> name

instance Serialize VariableDefinitons where
  serialize = parentheses

instance Serialize VariableDefiniton where
  serialize (VariableDefiniton variable type_ defaultValue) = variable <-> ":" <-> type_ <? defaultValue

instance Serialize Type where
  serialize = \case
    TypeNamedType x -> serialize x
    TypeListType      x -> serialize x

instance Serialize ListType where
  serialize = squares

instance Serialize DefaultValue where
  serialize (DefaultValue value) = "=" <-> value

