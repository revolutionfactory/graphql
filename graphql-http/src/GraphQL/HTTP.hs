module GraphQL.HTTP
  ( query
  , Document
  ) where

import Control.Monad.IO.Class (MonadIO(liftIO))
import Data.ByteString.Lazy (ByteString)
import Data.Function ((&))
import Data.Text.Encoding (encodeUtf8)
import GraphQL.Serialize (serialize)
import GraphQL.Types (Document)
import Network.HTTP.Client


query :: MonadIO m => Manager -> Request -> Document -> m (Response ByteString)
query manager request document = liftIO $ flip httpLbs manager $ request
  & setRequestMethod "POST"
  & setRequestBody (RequestBodyBS $ encodeUtf8 $ serialize document)
  where
  setRequestBody b r = r {requestBody = b}
  setRequestMethod m r = r {method = m}

